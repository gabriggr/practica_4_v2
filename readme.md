# Práctica 4 - Spark Set up
## Prerequisitos
## Organización del proyecto
El proyecto está compuesto por dos módulos:
1.  **practica-vm** : Esté proyecto esta preparado para que
podamos ejecutar contra la VM de cloudera. Nos permite leer y escribir las tablas de HIVE y HDFS que tenemos en la MV.
   - Para poder ejecutar necesitaremos añadir en ***Run/Debugs->VM options***

```
-Dspark.master=local[*]

```

2. **practica-local** : Proyecto para que podamos realizar ejecuciones locales
- Para poder ejecutar necesitaremos añadir en ***Run/Debugs->VM options***

```
-Dspark.master=local[*]
-Dderby.system.home=/home/bluetab/spark
-Dspark.sql.warehouse.dir=file:///home/bluetab/spark
```

  - Variables entorno windows
    1. Copiamos el directorio hadoop del proyecto(git) raiz a la ruta c:\
    2. Creamos la variable de entorno para nuestro Usuario

      ```HADOOP_HOME=C:\hadoop ```

  - **JDK 8**
  Para poder compilar el proyecto tenemos que tener instalado la jdk8 y configurar intellij para que la utilice

  file>>Project Structure>> Poject Setting>> Projecet>>Project SDK>>


## SPARK API

- [Spark API Funciones](https://spark.apache.org/docs/2.3.3/api/scala/index.html#org.apache.spark.sql.functions)
- [Spark API DataFrame](https://spark.apache.org/docs/2.3.3/api/scala/index.html#org.apache.spark.sql.Dataset)
- [Spark API SparkSession](https://spark.apache.org/docs/2.3.3/api/scala/index.html#org.apache.spark.sql.SparkSession)


# PRÁCTICA 4 - EJECICIOS

## 1. Masterización de la tabla loans

En está práctica vamos a crear un proceso spark para la masterización de le tabla loans, es decir vamos a pasar los datos
de la tabla de raw a la tabla de master  que habíamos creado en la práctica2


### Notas

- Antes de empezar actualizamos el repositorio con el nuevo código
```git
git pull
```
-   Origen y destino

RAW | MASTER
------------ | -------------
r_operations.t_loans | m_operations.t_loans

### Organización del Código


Clase | Descripción
------------ | -------------
***SparkApp*** | Clase base para los procesos, inicializa spark session
***AppRawToMaster*** | Aplicación principal lanza las cargas de raw a master
***Constants***| Trait contiene las constantes del proceso (schema,tablas y conf)
***Common*** | Contiene funciones comumnes, por ejemplo, lectura y escritura entre las capas
***RawToMaster*** | Trait que contiene todas las funciones de carga de las tablas de la capa raw a capa master


### Implemetación de Common, funciones de lectura y escritura
Estas implemetaciones la realizaremos en el trait ***bluetab.courses.spark.practica4.Common***


1.Función de lectura de tablas de raw ***readRawTable***,en nuestro data lake las tablas de raw siempre estarán particionadas por ***indate***.
```scala
val df = spark.read.table(table)
if (!inDate.isEmpty) df.filter(col("indate") === inDate) else df
```
2.Función de escritura en master ***saveTable***:
```scala
logger.debug("Save Table")
// Orden de la tabla original, para evitar errores.
val columns = spark.table(table).columns.map(col(_))
df
  .select(columns: _*)
  .repartition(WriteRepartition)
  .write
  .mode(mode)
  .insertInto(table)
```
### Implementación del método de Raw a Master para la tabla Loan
Estas implemetaciones las realizaremos en el trait
***bluetab.courses.spark.practica3.RawToMaster*** en la función ***rawToMasterLoan***

1.Lectura de la tabla de raw *t_loans*
```scala
logger.info("Start rawToMasterLoan")
import spark.implicits._
logger.debug("Read table")
val df = readRawTable(RTLoans)
df.show
```
2.Eliminamos la columna fecha de carga ***indate***
```scala
logger.debug("Drop indate")
val dfSinPart = df.drop("indate")
dfSinPart.show
```
3.Convertimos la fecha ***date*** a timestamp, para ello utilizamos la función de columna ***to_timestamp***
```scala
logger.debug("date -> to_timestamp")
val dfTime = dfSinPart.withColumn("date", to_timestamp($"date", "yyyy-MM-dd"))
dfTime.show
```
4.Cremos dos nuevas columnas ***year*** and ***month***,que serán las columas de particionado junto con la columna ***status***.
```scala
logger.debug("Create partitions year and month")
 val dfPart = dfTime
   .withColumn("year", year($"date"))
   .withColumn("month", month($"date"))
 dfPart.show
```
5.Guardamos la tabla con el método implementado anteriormete
```scala
saveTable(MTLoans, dfPart)
logger.info("End rawToMasterLoan")
```

# Ejercicio 2 Generación de fichero de salida de la tabla transaction#

En ensta práctica vamos a generar un fichero de salida después de realizar unas transformaciones de la la tabla transactions.



### 1. Creamos Estructura para la práctica 4 en nuestro workSpace###

* **Package**: Para crear el package utilizaremos el asistente de intellij, sobre la carpeta /src/main/scala, botón derecho-New-Package el nombre del ***bluetab.courses.spark.practica4.output***
* **AppOuputTransactions**: Como en el punto anterior creamos una nueva clase, selectionamos tipo object y al llamaremos ***AppOuputTransactions***
* **OuputTransaction**: Crearemos un trait donde guardaremos la funciones para las transformaciones. La creación similar al punto 2




## 2. Creación de la App ***AppOuputTransactions***

```scala
object AppOuputTransactions extends SparkApp with OutputTransaction {

  private val logger = Logger.getLogger(AppOuputTransactions.getClass)

  val year = args(0).toInt

  logger.info(s"Start Output Transaction year $year")

  // Generación salida por año
  outputYear(year)(spark)

  logger.info(s"End Output Transaction year")

}

```

***Para la ejecución no tenemos que olvidar añadir***

VM: -Dspark.master=local[*]

Como argumento del proceso pasaremos el año : 2018

### 3. Lectura de Master de la tabla trasaction ###

* En este punto vamos a crear una función para la lectura de la tabla de transaciones,esta función la tendremos dentro del trait ***OutputTransaction***:


```scala
def readMasterTable(table: String)(implicit spark: SparkSession): DataFrame = {
}
```


### 4.Función de transformación###

#### Campos de salida ####
1. La salida tiene que realizarse por año como parámetro de entrada en año ej: 2018
2. El fichero de salida no tiene que contener los campos "bank", "account", "year", "month"
3. El Formato de la fecha tiene que ser dd-mm-yyyy
4. La columna tipo tiene que devolver los valores recodificados PRIJEM->A ,  VYDAJ->B  Otros -> C
5. Se tiene que devolver un nuevo campo "diff" que se calcula como la difencia entre "balance" y el "amount"
6. Enmascarar el campo Cuenta mostrando los dos primeros digitos + "###"
7. Los campos tienen que tener el orden "account_id","trans_id","operation", "amount","balance","diff","k_symbol","type","date"



```scala
def outputYear(year: Int)(implicit spark: SparkSession): Unit = {

    import spark.implicits._

    // 0. Leer tabla de master
    val df = readMasterTable("m_operations.t_transaction")

    // 1. Filtrar por el año


    // 2. Drop ó select Campos no pedidos para la salida ("bank","account","year", "month")


    // 3. Formato de Fecha en dd-MM-YYYY funcion date_format


    // 4. Cambiamos valor tipo  PRIJEM->A ,  VYDAJ->B  Otros -> C


    // 5. Nueva columna diff con la diferencia entre (balance-amount)


    // 6. Enmascarar Cuenta



    // 8 Orden de los campos "account_id","trans_id","operation", "amount","balance","diff","k_symbol","type","date"


    // 9 Guardar como transaction_out_<yyyy>

    val path = StagingOut + "transaction_out_" + year

    //writeOutput(path,dfOrder)



  }

```


### 5.Guarda la salida ###

#### Fichero de salida ####

* La salidata tiene que ser un fichero csv
* El separador tiene que ser ";"
* Tiene que incluir la cabecera
* El nombre del fichero de salida tiene que ser transaction_out_<yyyy>

```scala
  def writeOutput(path: String, df: DataFrame)(implicit spark: SparkSession): Unit = {

    // https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.DataFrameWriter

    ???
  }
```



