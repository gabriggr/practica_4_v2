#!/bin/bash
spark-submit \
--class  ${Classname} \
--name AppRawToMaster \
--master yarn \
--deploy-mode client \
--conf "spark.driver.extraJavaOptions=-Dlog4j.configuration=file:log4j.properties" \
--files "$HOME/log4j.properties" \
practica-vm-1.0-SNAPSHOT-jar-with-dependencies.jar \
${SpakParam}