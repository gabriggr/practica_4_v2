package bluetab.courses.spark.practica.master

import bluetab.courses.spark.practica.SparkApp
import org.apache.log4j.Logger

import scala.util.{Failure, Success, Try}

object AppRawToMaster extends SparkApp with RawToMaster {

  private val logger = Logger.getLogger(AppRawToMaster.getClass)

  val inDate = args(0)

  logger.info(s"Start RAW to MASTER Process load date: $inDate")


  val masterSteps: List[(String, String => Unit)] = List(
    ("1. rawToMasterLoan", rawToMasterLoan _),
    ("2. rawToMasterClient", rawToMasterClient _),
    ("3. rawToMasterAccount", rawToMasterAccount _),
    ("4. rawToMasterOrder", rawToMasterOrder _),
    ("5. rawToMasterDistrict", rawToMasterDistrict _),
    ("6. rawToMasterCard", rawToMasterCard _),
    ("7. rawToMasterDisp", rawToMasterDisp _),
    ("8. rawToMasterTransaction", rawToMasterTransaction _)
  )


  logger.info("Execute the work loads")
  masterSteps.foreach(f =>
    Try(f._2(inDate)) match {
      case Success(_) => logger.info(s"${f._1} => process end Ok")
      case Failure(ex) => logger.error(s"${f._1} =>  ${ex}")
    })


  logger.info("End RAW to MASTER Process")
}
