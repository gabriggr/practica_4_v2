package bluetab.courses.spark.practica.output

import bluetab.courses.spark.practica.SparkApp
import org.apache.log4j.Logger

object AppOuputTransactions extends SparkApp with OutputTransaction {

  private val logger = Logger.getLogger(AppOuputTransactions.getClass)

  val year = args(0).toInt

  logger.info(s"Start Output Transaction year $year")

  // Generación salida por año
  outputYear(year)(spark)

  logger.info(s"End Output Transaction year")

}