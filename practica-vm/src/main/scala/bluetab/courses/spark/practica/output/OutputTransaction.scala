package bluetab.courses.spark.practica.output

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import bluetab.courses.spark.practica.Constants
import org.apache.spark.sql.functions._

trait OutputTransaction extends Constants {
  def readMasterTable(table: String)(implicit spark: SparkSession): DataFrame = {
    spark.read.table(table)
  }

  def outputYear(year: Int)(implicit spark: SparkSession): Unit = {

    import spark.implicits._

    // 0. Leer tabla de master
    val df = readMasterTable("m_operations.t_transaction")
      .filter(col("year") === year)
      .drop("bank", "account", "year", "month")
      .withColumn("date", date_format($"date", "dd-MM-yyyy"))
      .withColumn("type",
        when(col("type") === "PRIJEM", "A")
        .otherwise(when(col("type") === "VYDAJ", "D").otherwise("C"))
      ) .withColumn("diff", col("balance") - col("amount"))
      .withColumn("account_id", concat_ws("", substring(col("account_id"), 0, 2), lit("###")))
      .select("account_id", "trans_id", "operation", "amount", "balance", "diff", "k_symbol", "type", "date")

    // 9 Guardar como transaction_out_<yyyy>
    val path = StagingOut + "transaction_out_" + year
    writeOutput(path, df)

  }

  def writeOutput(path: String, df: DataFrame)(implicit spark: SparkSession): Unit = {

    // https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.DataFrameWriter

    df
      .repartition(1)
      .write
      .mode(SaveMode.Overwrite)
      .option("sep", ";")
      .option("header", true)
      .csv(path)
  }

}
