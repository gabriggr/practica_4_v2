package bluetab.courses.spark.practica.master

import bluetab.courses.spark.practica.Common
import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, month, to_timestamp, year}
import org.apache.spark.sql.types._


trait RawToMaster extends Common {


  private val logger = Logger.getLogger(classOf[RawToMaster])

  /**
   * Método de ingesta de la tabla raw a la tabla de master
   *
   * @param inDate Fecha de particionado de la tabla de raw
   * @param spark  sesión de spark
   */
  def rawToMasterLoan(inDate: String)(implicit spark: SparkSession): Unit = {

    logger.info("Start rawToMasterLoan")
    import spark.implicits._
    logger.debug("Read table")
    val df = readRawTable(RTLoans)

    logger.debug("Drop indate and first column")
    val firstColName = df.columns(0)
    val dfSinPart = df
      .drop("indate")
      .filter(col(firstColName) =!= firstColName)


    logger.debug("date -> to_timestamp")
    val dfTime = dfSinPart.withColumn("date", to_timestamp($"date", "yyyy-MM-dd"))


    logger.debug("Create partitions year and month")
    val dfPart = dfTime
      .withColumn("year", year($"date"))
      .withColumn("month", month($"date"))

    dfPart.show()
    dfPart.printSchema()

    saveTable(MTLoans, dfPart)
    logger.info("End rawToMasterLoan")
  }

  /**
   * Método de ingesta Client de la capa raw a master
   * Esta tabla no está particionada
   *
   * @param inDate Fecha de particionado de la tabla en raw
   * @param spark  sesión de spark
   */
  def rawToMasterClient(inDate: String)(implicit spark: SparkSession): Unit = {
    logger.info("Start rawToMasterClient")
    import spark.implicits._
    logger.debug("Read table")
    val df = readRawTable(RTClient, inDate)

    logger.debug("Drop indate")
    val dfSinPart = df
      .drop("indate")
      .filter(col(df.columns(0))=!=df.columns(0))

    logger.debug("date -> to_timestamp")
    val dfTime = dfSinPart.withColumn("birth", to_timestamp($"birth", "yyyy-MM-dd"))

    saveTable(MTClient, dfTime)
    logger.info("End rawToMasterClient")

  }

  /**
   * Método de ingesta Account de la capa raw a master
   * Esta tabla no está particionada
   *
   * @param inDate Fecha de particionado de la tabla en raw
   * @param spark  sesión de spark
   */
  def rawToMasterAccount(inDate: String)(implicit spark: SparkSession): Unit = {

    logger.info("Start rawToMasterAccount")
    import spark.implicits._
    logger.debug("Read table")
    val df = readRawTable(RTAccount, inDate)

    logger.debug("Drop indate")
    val dfSinPart = df
      .drop("indate")
      .filter(col(df.columns(0))=!=df.columns(0))

    logger.debug("date -> to_timestamp")
    val dfTime = dfSinPart.withColumn("date", to_timestamp($"date", "yyyy-MM-dd"))

    saveTable(MTAccount, dfTime)
    logger.info("End rawToMasterAccount")

  }

  /**
   * Método de ingesta Orders de la capa raw a master
   * Esta tabla no está particionada
   *
   * @param inDate Fecha de particionado de la tabla en raw
   * @param spark  sesión de spark
   */
  def rawToMasterOrder(inDate: String)(implicit spark: SparkSession): Unit = {

    logger.info("Start rawToMasterLoan")
    import spark.implicits._
    logger.debug("Read table")
    val df = readRawTable(RTOrder)

    //spark.conf.set("spark.sql.parquet.writeLegacyFormat", true)
    val dfEnd = df
      .drop("indate")
      .filter(col(df.columns(0)) =!= df.columns(0))
      .withColumn("order_id", col("order_id").cast(IntegerType))
      .withColumn("account_id", col("account_id").cast(IntegerType))
      .withColumn("account_to", col("account_to").cast(IntegerType))
      .withColumn("amount", col("amount").cast("decimal(10,2)"))

    saveTable(MTOrder, dfEnd)
  }

  /**
   * Método de ingesta District de la capa raw a master
   * Esta tabla no está particionada
   *
   * @param inDate Fecha de particionado de la tabla en raw
   * @param spark  sesión de spark
   */
  def rawToMasterDistrict(inDate: String)(implicit spark: SparkSession): Unit = {
    logger.info("Start rawToMasterDistrict")
    logger.debug("Read table")
    val df = readRawTable(RTDistrict, inDate)

    logger.debug("Drop indate")
    val dfSinPart = df
      .drop("indate")
      .filter(col(df.columns(0)) =!= df.columns(0))

    dfSinPart.show

    saveTable(MTDistrict, dfSinPart)
    logger.info("End rawToMasterDistrict")
  }

  /**
   * Método de ingesta Card de la capa raw a master
   * Esta tabla particionada por type
   *
   * @param inDate Fecha de particionado de la tabla en raw
   * @param spark  sesión de spark
   */
  def rawToMasterCard(inDate: String)(implicit spark: SparkSession): Unit = {
    logger.info("Start rawToMasterCard")
    logger.debug("Read table")
    val df = readRawTable(RTCard, inDate)

    logger.debug("Drop indate")
    val dfSinPart = df
      .drop("indate")
      .filter(col(df.columns(0)) =!= df.columns(0))
      .withColumn("issued", to_timestamp(col("issued"), "yyyy-MM-dd"))


    dfSinPart.show

    saveTable(MTCard, dfSinPart)
    logger.info("End rawToMasterCard")

  }

  /**
   * Método de ingesta Disp de la capa raw a master
   * Esta tabla particionada por type
   *
   * @param inDate Fecha de particionado de la tabla en raw
   * @param spark  sesión de spark
   */
  def rawToMasterDisp(inDate: String)(implicit spark: SparkSession): Unit = {
    logger.info("Start rawToMasterDisp")
    logger.debug("Read table")
    val df = readRawTable(RTDisp, inDate)

    logger.debug("Drop indate")
    val dfSinPart = df
      .drop("indate")
      .filter(col(df.columns(0)) =!= df.columns(0))

    dfSinPart.show

    saveTable(MTDisp, dfSinPart)
    logger.info("End rawToMasterDisp")

  }

  /**
   * Método de ingesta Transaction de la capa raw a master
   * year: INT
   * month: INT
   * type : STRING
   * Hay que eliminar los datos duplicados de la tabla
   *
   * @param inDate Fecha de particionado de la tabla en raw
   * @param spark  sesión de spark
   */
  def rawToMasterTransaction(inDate: String)(implicit spark: SparkSession): Unit = {
    logger.info("Start rawToMasterTransaction")
    import spark.implicits._
    logger.debug("Read table")
    val df = readRawTable(RTTransaction, inDate)

    logger.debug("Drop indate")
    val dfSinPart = df.drop("indate").filter(col(df.columns(0)) =!= df.columns(0))

    logger.debug("date -> to_timestamp")
    val dfTime = dfSinPart.withColumn("date", to_timestamp($"date", "yyyy-MM-dd"))

    logger.debug("Create partitions year and month")
    val dfPart = dfTime
      .withColumn("year", year($"date"))
      .withColumn("month", month(col("date"))).distinct()

    saveTable(MTTransaction, dfPart)
    logger.info("End rawToMasteroan")
  }
}
