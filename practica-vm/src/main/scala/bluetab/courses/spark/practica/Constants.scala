package bluetab.courses.spark.practica

trait Constants {

  //// Schemas
  val MOperationsSchema = "m_operations"
  val ROperationsSchema = "r_operations"

  //// TABLES
  // Tables Master
  val MTLoans = s"${MOperationsSchema}.t_loan"
  val MTClient = s"${MOperationsSchema}.t_client"
  val MTAccount = s"${MOperationsSchema}.t_account"
  val MTTransaction = s"${MOperationsSchema}.t_transaction"
  val MTOrder = s"${MOperationsSchema}.t_order"
  val MTDistrict = s"${MOperationsSchema}.t_district"
  val MTCard = s"${MOperationsSchema}.t_card"
  val MTDisp = s"${MOperationsSchema}.t_disp"

  // Tables Raw
  val RTLoans = s"${ROperationsSchema}.t_loan"
  val RTClient = s"${ROperationsSchema}.t_client"
  val RTAccount = s"${ROperationsSchema}.t_account"
  val RTTransaction = s"${ROperationsSchema}.t_transaction"
  val RTOrder = s"${ROperationsSchema}.t_order"
  val RTDistrict = s"${ROperationsSchema}.t_district"
  val RTCard = s"${ROperationsSchema}.t_card"
  val RTDisp = s"${ROperationsSchema}.t_disp"

   //// Configs
  val WriteRepartition = 4


  val StagingOut:String = "/curso/staging/out/"
}
