package bluetab.courses.spark.practica

import bluetab.courses.spark.practica.master.RawToMaster
import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions.col

trait Common extends Constants {
  private val logger = Logger.getLogger(classOf[RawToMaster])

  def readRawTable(table: String, inDate: String = "")(implicit spark: SparkSession): DataFrame = {

    val df = spark.read.table(table)
    if (!inDate.isEmpty) df.filter(col("indate") === inDate) else df
  }

  def saveTable(table: String, df: DataFrame, mode: SaveMode = SaveMode.Overwrite)(implicit spark: SparkSession): Unit = {

    logger.debug("Save Table")
    // Orden de la tabla original, para evitar errores.
    val columns = spark.table(table).columns.map(col(_))
    df
      .select(columns: _*)
      .repartition(WriteRepartition)
      .write
      .mode(mode)
      .insertInto(table)

  }
}