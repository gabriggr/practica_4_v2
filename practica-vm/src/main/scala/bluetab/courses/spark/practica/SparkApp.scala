package bluetab.courses.spark.practica

import org.apache.spark.sql.SparkSession

trait SparkApp extends App {

  implicit lazy val spark = {
    val session =
      SparkSession
        .builder
        .enableHiveSupport
        .getOrCreate

    session.sqlContext.setConf("hive.exec.dynamic.partition", "true")
    session.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
    session.sqlContext.setConf("spark.sql.storeAssigmentPolicy", "LEGACY")
    // session.conf.set("spark.sql.parquet.writeLegacyFormat", true)
     session.conf.set("spark.sql.storeAssignmentPolicy", "LEGACY")


    session
  }
}
