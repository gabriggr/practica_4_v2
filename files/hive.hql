CREATE DATABASE IF NOT EXISTS r_operations 
COMMENT 'Raw Database of operations' 
LOCATION '/curso/raw/operations';

CREATE DATABASE IF NOT EXISTS m_operations
COMMENT "Master Database of operations"
LOCATION "/curso/master/operations";

CREATE DATABASE IF NOT EXISTS st_operations
COMMENT "Staging Database"
LOCATION "/curso/staging/";


CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_transaction(
      trans_id STRING ,
      account_id STRING ,
      `date` STRING ,
      type STRING ,
      operation STRING ,
      amount STRING ,
      balance STRING ,
      k_symbol STRING ,
      bank STRING,
      account STRING ,
      id STRING 
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/transactions'
TBLPROPERTIES ("skip.header.line.count"="1");

CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_transaction(
      trans_id STRING ,
      account_id STRING ,
      `date` STRING ,
      type STRING ,
      operation STRING ,
      amount STRING ,
      balance STRING ,
      k_symbol STRING ,
      bank STRING,
      account STRING ,
      id STRING 
    )
partitioned by (indate string)
STORED AS AVRO;


CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_transaction(
      trans_id INT,
      account_id INT,
      `date` TIMESTAMP,
      operation STRING,
      amount DECIMAL(10,2),
      balance DECIMAL(10,2),
      k_symbol STRING,
      bank STRING,
      account INT,
      id INT
    )
partitioned by (year INT, month INT, type STRING)
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");


CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_disp(
     disp_id STRING ,
     client_id STRING ,
     account_id STRING,
     type STRING
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/disps'
TBLPROPERTIES ("skip.header.line.count"="1");

CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_disp(
     disp_id STRING ,
     client_id STRING ,
     account_id STRING,
     type STRING
    )
partitioned by (indate string)
STORED AS AVRO;

CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_disp(
     disp_id INT ,
     client_id INT,
     account_id INT,
     type STRING
    )
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");

CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_district(
     disp_id STRING,
     name STRING,
     region STRING,
     A4 STRING,
     A5 STRING,
     A6 STRING,
     A7 STRING,
     A8 STRING,
     A9 STRING,
     A10 STRING,
     A11 STRING,
     A12 STRING,
     A13 STRING,
     A14 STRING,
     A15 STRING,
     A16 STRING
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/districts'
TBLPROPERTIES ("skip.header.line.count"="1");

CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_district(
     disp_id STRING,
     name STRING,
     region STRING,
     A4 STRING,
     A5 STRING,
     A6 STRING,
     A7 STRING,
     A8 STRING,
     A9 STRING,
     A10 STRING,
     A11 STRING,
     A12 STRING,
     A13 STRING,
     A14 STRING,
     A15 STRING,
     A16 STRING
    )
partitioned by (indate string)
STORED AS AVRO;

CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_district(
     disp_id INT,
     name STRING,
     region STRING,
     A4 INT,
     A5 INT,
     A6 INT,
     A7 INT,
     A8 INT,
     A9 INT,
     A10 DOUBLE,
     A11 INT,
     A12 STRING,
     A13 DOUBLE,
     A14 INT,
     A15 STRING,
     A16 INT
    )
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");


CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_order (
     order_id STRING ,
     account_id STRING ,
     bank_to STRING,
     account_to STRING,
     amount STRING,
     k_symbol STRING
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/orders'
TBLPROPERTIES ("skip.header.line.count"="1");

CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_order (
     order_id STRING ,
     account_id STRING ,
     bank_to STRING,
     account_to STRING,
     amount STRING,
     k_symbol STRING
    )
partitioned by (indate string)
STORED AS AVRO;

CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_order (
     order_id INT ,
     account_id INT ,
     bank_to STRING,
     account_to INT,
     amount DECIMAL(10,2),
     k_symbol STRING
    )
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");

CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_card (
     card_id STRING ,
     disp_id STRING ,
     type STRING,
     issued STRING
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/cards'
TBLPROPERTIES ("skip.header.line.count"="1");


CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_card (
     card_id STRING ,
     disp_id STRING ,
     type STRING,
     issued STRING
    )
partitioned by (indate string)
STORED AS AVRO;

CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_card (
     card_id INT ,
     disp_id INT ,
     issued STRING
    )
partitioned by(type STRING)
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");


CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_account (
     account_id STRING ,
     district_id STRING ,
     frequency STRING,
     `date` STRING
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/accounts'
TBLPROPERTIES ("skip.header.line.count"="1");

CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_account (
     account_id STRING ,
     district_id STRING ,
     frequency STRING,
     `date` STRING
    )
partitioned by (indate string)
STORED AS AVRO;

CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_account (
     account_id INT ,
     district_id INT ,
     frequency STRING,
     `date` TIMESTAMP
    )
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");

CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_client (
    client_id STRING ,
    birth STRING ,
    district_id STRING,
    sex STRING,
    name STRING,
    surname STRING,
    phone STRING
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/clients'
TBLPROPERTIES ("skip.header.line.count"="1");

CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_client (
    client_id STRING ,
    birth STRING ,
    district_id STRING,
    sex STRING,
    name STRING,
    surname STRING,
    phone STRING
    )
partitioned by (indate string)
STORED AS AVRO;

CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_client (
    client_id INT ,
    birth TIMESTAMP,
    district_id INT,
    sex VARCHAR(1),
    name STRING,
    surname STRING,
    phone INT
    )
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");


CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_loan (
    loan_id STRING ,
    account_id STRING ,
    `date` STRING,
    amount STRING,
    duration STRING,
    payments STRING,
    status STRING
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/loans'
TBLPROPERTIES ("skip.header.line.count"="1");

CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_loan(
  loan_id STRING COMMENT "identification of the account",
  account_id STRING COMMENT "identification of the account",
  `date` STRING COMMENT "date when the loan was granted",
  amount STRING COMMENT "amount of money",
  duration STRING COMMENT "duration of the loan",
  payments STRING COMMENT "monthly payments",
  status STRING COMMENT "status of paying off the loan")
  COMMENT "This table contains the nformation about loans"
partitioned by (indate string)
STORED AS AVRO;

CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_loan(
  loan_id INT COMMENT "identification of the account",
  account_id INT COMMENT "identification of the account",
  `date` TIMESTAMP COMMENT "date when the loan was granted",
  amount DECIMAL(10,2) COMMENT "amount of money",
  duration INT COMMENT "duration of the loan",
  payments DECIMAL(10,2) COMMENT "monthly payments"
  ) COMMENT "This table contains the information about loans"
partitioned by (year INT, month INT, status STRING)
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");



