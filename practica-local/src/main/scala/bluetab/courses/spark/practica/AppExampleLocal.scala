package bluetab.courses.spark.practica

import org.apache.spark.sql.SparkSession

object AppExampleLocal extends App with ExampleLocal {

  /**
    *  Parámetros para la ejecución
    *  Paramétros VM:
    * -Dspark.master=local[*]
    * -Dderby.system.home=/home/bluetab/spark
    * -Dspark.sql.warehouse.dir=file:////home/bluetab/spark
    */

  var csvPath = "./practica-local/src/main/resources/cards.csv"
  // Create spark Sessión
  implicit val spark = SparkSession
    .builder
    //.enableHiveSupport
    .getOrCreate

  val df = readCsv(csvPath)

  df.write.saveAsTable("tabl1")

  df.show()

}
