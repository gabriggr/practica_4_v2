package bluetab.courses.spark.practica

import org.apache.spark.sql.{DataFrame, SparkSession}

trait ExampleLocal {

  def readCsv(fileName: String)(implicit spark: SparkSession): DataFrame = {
    spark
      .read
      .option("header", true)
      .option("inferSchema", true)
      .csv(fileName)
  }


}
