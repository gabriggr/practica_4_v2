package bluetab.courses.spark.practica

import org.apache.spark.sql.SparkSession
import org.scalatest.flatspec.AnyFlatSpec


class ExampleLocalTest extends AnyFlatSpec with ExampleLocal {



  // Create spark Sessión
  implicit val spark = SparkSession
    .builder
      .config("driver-memory","512m")
    .master("local")
    .getOrCreate

  "readCsv" should "load dataframe from csv file" in {
    val csvFile = getClass.getResource("/test.cvs").getPath
    assert(readCsv(csvFile).count == 2)
  }

  it should "load Error" in {
    val csvFile = getClass.getResource("/test.cvs").getPath
    assert(readCsv(csvFile).count >= 2)
  }
}
