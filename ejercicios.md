# Práctica 4 - EJERCICIOS

### Ejercicio 1 Implemetación de la carga de master para todas las tablas de las práctica

Hay que realizar la implementación de todos los procesos de las tablas de la capa RAW a la capa MASTER.

Los métodos que tenemos que implementar son :

```scala

  def rawToMasterClient(inDate: String)(implicit spark: SparkSession): Unit = ???

  def rawToMasterAccount(inDate: String)(implicit spark: SparkSession): Unit =   ???

  def rawToMasterOrder(inDate: String)(implicit spark: SparkSession): Unit = ???

  def rawToMasterDistrict(inDate: String)(implicit spark: SparkSession): Unit = ???
  def rawToMasterDisp(inDate: String)(implicit spark: SparkSession): Unit = ???
  def rawToMasterTransaction(inDate: String)(implicit spark: SparkSession): Unit = ???

```

***Hay que tener en cuenta los campos de partición, si los tienen, asi como añadir las columnas necesarias para estos.***


***La tabla de transacciones contiene duplicados, es necesario eliminarlos antes de realizar la carga en master***


# Ejercicio 2 -Generación de salida #

Crear un nuevo proceso que genere la salida para la transacciones por tipo y año

### Función de transformación###

#### Campos de salida ####

1. La salida tiene que realizarse por tipo y por año,por tanto los parámetros de entrada ej: VYDAJ 2018
2. Solo se tiene que devolver transacciones con un ammount > 100
3. Nueva columna "currency", para las operaciones  "VKLAD" y "VYBER" sera "USD" en cualquier otro caso "EUR"
4. El fichero de salida no tiene que contener los campos "type", "year", "month"
5. El Formato de la fecha tiene que ser yyyy/dd/mm
6. La columna operation tiene que ser recodificada:

    |Valor Actual|Valor salida
    |-------------|-----------------------------|
    |VYBER KARTOU| Credit card withdrawal|
    |VKLAD| Credit in cash|
    |PREVOD Z UCTU| Collection from another bank|
    |VYBER|Withdrawal in cash|
    |PREVOD NA UCET| Remittance to another bank|
    |Otro| Unknow desciption|

7. Nuevo campo status para el en función del balance:

    |balance|       status
    |-------------|----------|
    |0 < balance      | red  |
    |0 <= balace <2000 | orange|
    |2000 <= balance < 30000    | green|
    |balance >= 30000 | gold|

8. Los amounts y los balances superiores a 60000 tienen que ocultarse "XXXXX"
9. Enmascarar el campo Cuenta mostrando los dos primeros digitos + "###"
10. Los campos tienen que tener el orden "account_id","trans_id","operation", "amount","balance","currency", "status","k_symbol","bank","account","date"

#### Fichero de salida ####

* La salida tiene que ser un fichero "json"
* El nombre del fichero de salida tiene que ser transaction_out_<type>_<yyyy>.json

